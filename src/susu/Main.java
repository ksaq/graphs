package susu;

import javafx.util.Pair;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

class Main {

    public static void main(String[] args) {
        Graph graph = new Graph("Inputs\\lab2\\test3.txt");

        Graph prima = graph.getSpaingTreePrima();
        Graph kruscal = graph.getSpaingTreeKruscal();

        prima.writeGraph("prima.txt");
        kruscal.writeGraph("kruscal.txt");

    }
}
